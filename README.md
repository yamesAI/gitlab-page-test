![](https://elioway.gitlab.io/DEV/gitlab-page-test/elio-gitlab-page-test-logo.png)

> GitlabPageTest, **the elioWay**

# gitlab-page-test

Starter pack for an **elioWay** app using `yo thing`(<https://www.npmjs.com/package/generator-thing>)

- [gitlab-page-test Documentation](https://elioway.gitlab.io/DEV/gitlab-page-test/)

## Prerequisites

- [gitlab-page-test Prerequisites](https://elioway.gitlab.io/DEV/gitlab-page-test/installing.html)

## Installing

- [Installing gitlab-page-test](https://elioway.gitlab.io/DEV/gitlab-page-test/installing.html)

## Seeing is Believing

```
You're seeing it.
```

- [DEV Quickstart](https://elioway.gitlab.io/DEV/quickstart.html)
- [gitlab-page-test Quickstart](https://elioway.gitlab.io/DEV/gitlab-page-test/quickstart.html)

# Credits

- [gitlab-page-test Credits](https://elioway.gitlab.io/DEV/gitlab-page-test/credits.html)

## License

[MIT](license)

![](https://elioway.gitlab.io/DEV/gitlab-page-test/apple-touch-icon.png)
